package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class MinMaxTest {

    @Test
    public void numbersTest() throws Exception {

        int result1= new MinMax().number(1,2);
        assertEquals("MinMax numbers Test 1", 2, result1);

	int result2 = new MinMax().number(3,2);
	assertEquals("MinMax numbers Test 2", 2, result2);
    }

    @Test
    public void barTest() throws Exception {
	
	String result3 = new MinMax().bar("Grigoris");
	assertEquals("MinMax bar Test 1", "Grigoris", result3);

	String result4 = new MinMax().bar("");
	assertEquals("MinMax bar Test 2", "", result4);

    }
}

