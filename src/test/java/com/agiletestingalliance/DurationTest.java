package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class DurationTest {
    
    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Test
    public void durtest() throws Exception {

        String msg= new Duration().dur();
        assertEquals("Duration Test", "CP-DOF is designed specifically for corporates and working professionals alike. If you are a corporate and can't dedicate full day for training, then you can opt for either half days course or  full days programs which is followed by theory and practical exams.", msg);

    }

    @Test
    public void calculateIntValuetest() {
	System.out.print("5");
        assertEquals("5", systemOutRule.getLog());    	
    }
}

